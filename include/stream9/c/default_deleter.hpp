#ifndef STREAM9_C_DEFAULT_DELETER_HPP
#define STREAM9_C_DEFAULT_DELETER_HPP

#include <stdlib.h>

namespace stream9::c {

struct default_deleter {
    template<typename T>
    void operator()(T* const ptr) const noexcept
    {
        ::free(ptr);
    }

    template<typename T>
    void operator()(T const* const ptr) const noexcept
    {
        ::free(const_cast<T*>(ptr));
    }
};

} // namespace stream9::c

#endif // STREAM9_C_DEFAULT_DELETER_HPP
