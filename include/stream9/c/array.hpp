#ifndef STREAM9_C_POINTER_ARRAY_HPP
#define STREAM9_C_POINTER_ARRAY_HPP

#include "default_deleter.hpp"
#include "namespace.hpp"

#include <cassert>
#include <compare>
#include <concepts>
#include <iterator>
#include <memory>
#include <type_traits>
#include <utility>

#include <stdlib.h>

#include <stream9/iterators.hpp>
#include <stream9/ranges/lexicographical_compare_three_way.hpp>

#include <iostream>

namespace stream9::c {

template<typename, typename> class array_iterator;

template<typename T>
struct array_traits
{
    static bool is_sentinel(T const& v) noexcept
    {
        return  v == T();
    }

    using deleter = default_deleter;
};

template<typename T, typename Traits = array_traits<T>>
class array
{
public:
    using value_type = T;
    using reference = T&;
    using const_reference = T const&;
    using pointer = T*;
    using const_pointer = T const*;
    using iterator = array_iterator<T, Traits>;
    using const_iterator = array_iterator<T const, Traits>;
    using sentinel_t = std::default_sentinel_t;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

public:
    // essential
    array() = default;

    explicit array(pointer);

    ~array() noexcept;

    array(array const&) = delete;
    array& operator=(array const&) = delete;

    array(array&&) = default;
    array& operator=(array&&) = default;

    void swap(array&) noexcept;

    // accessor
    iterator       begin() noexcept;
    const_iterator begin() const noexcept;
    sentinel_t     end() const noexcept;

    reference       operator[](difference_type) noexcept;
    const_reference operator[](difference_type) const noexcept;

    reference       front() noexcept;
    const_reference front() const noexcept;

    pointer       data() noexcept;
    const_pointer data() const noexcept;

    // query
    bool empty() const noexcept;
    size_type size() const noexcept;

    // modifier
    void reset(pointer arr) noexcept;
    pointer release() noexcept;

    // comparison
    bool operator==(array const&) const noexcept
        requires std::equality_comparable<T>;

    auto operator<=>(array const&) const noexcept
        requires std::three_way_comparable<T>;

private:
    std::unique_ptr<T, typename Traits::deleter> m_ptr;
};

template<typename T, typename Traits>
class array_iterator : public iter::iterator_facade<
                                        array_iterator<T, Traits>,
                                        std::contiguous_iterator_tag,
                                        T& >
{
public:
    array_iterator() = default;

    array_iterator(T* p) noexcept
        : m_ptr { p }
    {}

private:
    friend class iter::iterator_core_access;

    T& dereference() const noexcept { return *m_ptr; }

    void increment() noexcept { ++m_ptr; }
    void decrement() noexcept { --m_ptr; }

    void advance(std::ptrdiff_t const n) noexcept { m_ptr += n; }

    std::ptrdiff_t distance_to(array_iterator const& other) const noexcept
    {
        return other.m_ptr - m_ptr;
    }

    bool equal(array_iterator const& other) const noexcept
    {
        return m_ptr == other.m_ptr;
    }

    auto compare(array_iterator const& other) const noexcept
    {
        return m_ptr <=> other.m_ptr;
    }

    bool equal(std::default_sentinel_t const s) const noexcept
    {
        return std::is_eq(compare(s));
    }

    std::strong_ordering compare(std::default_sentinel_t) const noexcept
    {
        if (Traits::is_sentinel(*m_ptr)) {
            return std::strong_ordering::equivalent;
        }
        else {
            return std::strong_ordering::greater;
        }
    }

private:
    T* m_ptr = nullptr;
};

/*
 * definition
 */
template<typename T, typename D>
array<T, D>::
array(pointer const p)
    : m_ptr { p }
{}

template<typename T, typename D>
array<T, D>::
~array() noexcept
{
    if (m_ptr) {
        if constexpr (std::is_pointer_v<T>) {
            typename D::deleter d;
            for (auto& v: *this) {
                d(v);
            }
        }
        else {
            for (auto& v: *this) {
                v.~T();
            }
        }
    }
}

template<typename T, typename D>
void array<T, D>::
swap(array& other) noexcept
{
    using std::swap;
    swap(m_ptr, other.m_ptr);
}

template<typename T, typename D>
array<T, D>::iterator array<T, D>::
begin() noexcept
{
    return m_ptr.get();
}

template<typename T, typename D>
array<T, D>::const_iterator array<T, D>::
begin() const noexcept
{
    return m_ptr.get();
}

template<typename T, typename D>
array<T, D>::sentinel_t array<T, D>::
end() const noexcept
{
    return {};
}

template<typename T, typename D>
array<T, D>::reference array<T, D>::
operator[](difference_type const i) noexcept
{
    assert(m_ptr);
    return m_ptr.get()[i];
}

template<typename T, typename D>
array<T, D>::const_reference array<T, D>::
operator[](difference_type const i) const noexcept
{
    assert(m_ptr);
    return m_ptr.get()[i];
}

template<typename T, typename D>
array<T, D>::reference array<T, D>::
front() noexcept
{
    assert(m_ptr);
    return *m_ptr.get();
}

template<typename T, typename D>
array<T, D>::const_reference array<T, D>::
front() const noexcept
{
    assert(m_ptr);
    return *m_ptr.get();
}

template<typename T, typename D>
array<T, D>::pointer array<T, D>::
data() noexcept
{
    return m_ptr.get();
}

template<typename T, typename D>
array<T, D>::const_pointer array<T, D>::
data() const noexcept
{
    return m_ptr.get();
}

template<typename T, typename D>
bool array<T, D>::
empty() const noexcept
{
    return !m_ptr || begin() == end();
}

template<typename T, typename D>
array<T, D>::size_type array<T, D>::
size() const noexcept
{
    if (!m_ptr) return 0;

    size_type cnt = 0;
    auto e = end();

    for (auto it = begin(); it != e; ++it) {
        ++cnt;
    }

    return cnt;
}

template<typename T, typename D>
void array<T, D>::
reset(pointer const arr) noexcept
{
    m_ptr.reset(arr);
}

template<typename T, typename D>
array<T, D>::pointer array<T, D>::
release() noexcept
{
    return m_ptr.release();
}

template<typename T, typename D>
bool array<T, D>::
operator==(array const& other) const noexcept
    requires std::equality_comparable<T>
{
    return std::ranges::equal(*this, other);
}

template<typename T, typename D>
auto array<T, D>::
operator<=>(array const& other) const noexcept
    requires std::three_way_comparable<T>
{
    return rng::lexicographical_compare_three_way(*this, other);
}

} // namespace stream9::c

#endif // STREAM9_C_POINTER_ARRAY_HPP
