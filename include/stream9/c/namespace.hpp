#ifndef STREAM9_C_NAMESPACE_HPP
#define STREAM9_C_NAMESPACE_HPP

namespace std::ranges {}
namespace stream9::ranges {}
namespace stream9::iterators {}
namespace stream9::strings {}

namespace stream9::c {

namespace iter { using namespace stream9::iterators; }
namespace rng { using namespace std::ranges; }
namespace rng { using namespace stream9::ranges; }
namespace str { using namespace stream9::strings; }

} // namespace stream9::c

#endif // STREAM9_C_NAMESPACE_HPP
