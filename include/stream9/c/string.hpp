#ifndef STREAM9_C_STRING_HPP
#define STREAM9_C_STRING_HPP

#include "default_deleter.hpp"
#include "namespace.hpp"

#include <cassert>
#include <compare>
#include <cstddef>
#include <iosfwd>
#include <iterator>
#include <memory>
#include <memory_resource>
#include <string>
#include <string_view>
#include <type_traits>

#include <stream9/iterators.hpp>
#include <stream9/characters/concepts.hpp>
#include <stream9/strings/concepts.hpp>
#include <stream9/cstring_view.hpp>

namespace stream9::c {

template<character CharT,
         typename Deleter = default_deleter >
class basic_string
{
public:
    static_assert(!std::is_reference_v<CharT>);

    template<typename Value> class iterator_t;

    using value_type = CharT;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using pointer = CharT*;
    using const_pointer = CharT const*;
    using reference = CharT&;
    using const_reference = CharT const&;
    using iterator = iterator_t<CharT>;
    using const_iterator = iterator_t<CharT const>;
    using sentinel = std::default_sentinel_t;
    using comparison_category = decltype(CharT() <=> CharT());
    using string_view = std::basic_string_view<std::remove_cv_t<CharT>>;

public:
    // essential
    basic_string() = default;

    explicit basic_string(pointer p) noexcept;

    basic_string(std::nullptr_t) = delete;

    ~basic_string() = default;

    // If you want copy, convert it to std::string insted
    basic_string(basic_string const&) = delete;
    basic_string& operator=(basic_string const&) = delete;

    basic_string(basic_string&&) = default;
    basic_string& operator=(basic_string&&) = default;

    template<character C, typename D>
    friend void swap(basic_string<C, D>& lhs, basic_string<C, D>& rhs) noexcept;

    // accessor
    iterator       begin() noexcept;
    const_iterator begin() const noexcept;
    sentinel       end() const noexcept;

    reference       operator[](difference_type i) noexcept;
    const_reference operator[](difference_type i) const noexcept;

    // no at() because string isn't sized

    reference       front() noexcept;
    const_reference front() const noexcept;

    // no back() because string isn't sized

    pointer       data() noexcept;
    const_pointer data() const noexcept;

    // return "" if data() is nullptr
    const_pointer c_str() const noexcept;

    // query
    size_type size() const noexcept;
    bool empty() const noexcept;

    // modifier
    void reset(pointer) noexcept;
    pointer release() noexcept;

    // conversion

    // return ""sv if data() is nullptr
    operator string_view () const noexcept;

    operator basic_cstring_view<std::remove_cv_t<CharT>> () const noexcept;

    explicit operator std::basic_string<std::remove_cv_t<CharT>> () const;

    // comparison
    bool operator==(basic_string const&) const noexcept;

    comparison_category operator<=>(basic_string const&) const noexcept;

    bool operator==(std::convertible_to<string_view> auto const&) const noexcept;

    comparison_category
    operator<=>(std::convertible_to<string_view> auto const&) const noexcept;

    // streaming
    template<typename CharT2, typename Deleter2>
    friend std::basic_ostream<std::remove_cv_t<CharT2>>&
    operator<<(std::basic_ostream<std::remove_cv_t<CharT2>>&,
               basic_string<CharT2, Deleter2> const&) noexcept;

private:
    std::unique_ptr<CharT, Deleter> m_buf;
};

using string = basic_string<char>;
using const_string = basic_string<char const>;

template<character C, typename D>
template<typename Value>
class basic_string<C, D>::iterator_t
                    : public iter::iterator_facade<iterator_t<Value>,
                                                   std::contiguous_iterator_tag,
                                                   Value& >
{
public:
    iterator_t() = default;

    iterator_t(Value* const s) noexcept
        : m_ptr { s }
    {}

private:
    friend class iter::iterator_core_access;

    Value& dereference() const noexcept { return *m_ptr; }

    void increment() noexcept { ++m_ptr; }
    void decrement() noexcept { --m_ptr; }

    void advance(difference_type const n) noexcept { m_ptr += n; }

    difference_type distance_to(iterator_t const& other) const noexcept
    {
        return other.m_ptr - m_ptr;
    }

    bool equal(iterator_t const& other) const noexcept
    {
        return m_ptr == other.m_ptr;
    }

    auto compare(iterator_t const& other) const noexcept
    {
        return m_ptr <=> other.m_ptr;
    }

    bool equal(std::default_sentinel_t const s) const noexcept
    {
        return compare(s) == std::strong_ordering::equivalent;
    }

    std::strong_ordering compare(std::default_sentinel_t) const noexcept
    {
        if (!m_ptr || *m_ptr == C()) {
            return std::strong_ordering::equivalent;
        }
        else {
            return std::strong_ordering::greater;
        }
    }

private:
    Value* m_ptr = nullptr;
};

/*
 * definition
 */
template<character C, typename D>
basic_string<C, D>::
basic_string(pointer const p) noexcept
    : m_buf { p }
{
    assert(m_buf);
}

template<character C, typename D>
void
swap(basic_string<C, D>& lhs, basic_string<C, D>& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_buf, rhs.m_buf);
}

template<character C, typename D>
basic_string<C, D>::iterator basic_string<C, D>::
begin() noexcept
{
    return m_buf.get();
}

template<character C, typename D>
basic_string<C, D>::const_iterator basic_string<C, D>::
begin() const noexcept
{
    return m_buf.get();
}

template<character C, typename D>
basic_string<C, D>::sentinel basic_string<C, D>::
end() const noexcept
{
    return {};
}

template<character C, typename D>
basic_string<C, D>::reference basic_string<C, D>::
operator[](difference_type const i) noexcept
{
    assert(m_buf);
    return m_buf.get()[i];
}

template<character C, typename D>
basic_string<C, D>::const_reference basic_string<C, D>::
operator[](difference_type const i) const noexcept
{
    assert(m_buf);
    return m_buf.get()[i];
}

template<character C, typename D>
basic_string<C, D>::reference basic_string<C, D>::
front() noexcept
{
    assert(m_buf);
    return m_buf.get()[0];
}

template<character C, typename D>
basic_string<C, D>::const_reference basic_string<C, D>::
front() const noexcept
{
    assert(m_buf);
    return m_buf.get()[0];
}

template<character C, typename D>
basic_string<C, D>::pointer basic_string<C, D>::
data() noexcept
{
    return m_buf.get();
}

template<character C, typename D>
basic_string<C, D>::const_pointer basic_string<C, D>::
data() const noexcept
{
    return m_buf.get();
}

template<character C, typename D>
basic_string<C, D>::const_pointer basic_string<C, D>::
c_str() const noexcept
{
    return m_buf.get() ? m_buf.get() : "";
}

template<character C, typename D>
basic_string<C, D>::size_type basic_string<C, D>::
size() const noexcept
{
    if (m_buf.get() == nullptr) {
        return 0;
    }
    else {
        return str::size(m_buf.get());
    }
}

template<character C, typename D>
bool basic_string<C, D>::
empty() const noexcept
{
    return m_buf.get() == nullptr || front() == C();
}

template<character C, typename D>
void basic_string<C, D>::
reset(pointer const s) noexcept
{
    m_buf.reset(s);
}

template<character C, typename D>
basic_string<C, D>::pointer basic_string<C, D>::
release() noexcept
{
    return m_buf.release();
}

template<character C, typename D>
basic_string<C, D>::
operator basic_string<C, D>::string_view () const noexcept
{
    return m_buf.get() ? m_buf.get() : "";
}

template<character C, typename D>
basic_string<C, D>::
operator basic_cstring_view<std::remove_cv_t<C>> () const noexcept
{
    return m_buf.get();
}

template<character C, typename D>
basic_string<C, D>::
operator std::basic_string<std::remove_cv_t<C>> () const
{
    return m_buf.get() ? m_buf.get() : "";
}

template<character C, typename D>
bool basic_string<C, D>::
operator==(basic_string const& other) const noexcept
{
    return std::is_eq(*this <=> other);
}

template<character C, typename D>
basic_string<C, D>::comparison_category basic_string<C, D>::
operator<=>(basic_string const& other) const noexcept
{
    return str::compare_three_way(*this, other);
}

template<character C, typename D>
bool basic_string<C, D>::
operator==(std::convertible_to<string_view> auto const& other) const noexcept
{
    return std::is_eq(*this <=> other);
}

template<character C, typename D>
basic_string<C, D>::comparison_category basic_string<C, D>::
operator<=>(std::convertible_to<string_view> auto const& other) const noexcept
{
    return str::compare_three_way(*this, other);
}

template<typename C, typename D>
std::basic_ostream<std::remove_cv_t<C>>&
operator<<(std::basic_ostream<std::remove_cv_t<C>>& os,
           basic_string<C, D> const& s) noexcept
{
    return os << s.m_buf.get();
}

} // namespace stream9::c

namespace std {

template<typename C, typename D>
struct hash<stream9::c::basic_string<C, D>>
{
    size_t operator()(stream9::c::basic_string<C, D> const& s) const
    {
        return hash(std::basic_string_view<C>(s));
    }
};

} // namespace std

#endif // STREAM9_C_STRING_HPP
